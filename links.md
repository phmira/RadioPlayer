# Links and bitrates (kbps)
## Krokodyl
### 128
https://icecast4.play.cz/krokodyl128.mp3

### 64
https://icecast6.play.cz/krokodyl64.mp3

### 32
https://icecast4.play.cz/krokodyl32.mp3

---

## Rockradio
### 128
https://playerservices.streamtheworld.com/api/livestream-redirect/ROCK_RADIO.aac

### 64
https://playerservices.streamtheworld.com/api/livestream-redirect/ROCK_RADIOAAC.aac

---

## Rockradio - Hard & Heavy
### 128
https://ice2.abradio.cz/metalomanie128.aac

### 64
https://ice2.abradio.cz/metalomanie64.aac

---

## Rockradio - Alternative
### 128
https://ice2.abradio.cz/znamkapunku128.aac

### 64
https://ice.abradio.cz/znamkapunku64.aac

---

## Oldies Radio
### 128
https://ice2.abradio.cz/oldiesradio128.aac

### 64
https://ice2.abradio.cz/oldiesradio64.aac

---

## Radio Kiss
### 128
https://ice5.abradio.cz/kiss128.mp3

---

## Radio Beat
### 128
https://ice6.abradio.cz/beat128.aac

### 64
https://ice6.abradio.cz/beat64.aac

---

## Fajn Radio
### 128
https://playerservices.streamtheworld.com/api/livestream-redirect/FAJN_RADIO.aac

### 64
https://playerservices.streamtheworld.com/api/livestream-redirect/FAJN_RADIOAAC.aac

---

## Hitradio City
### 128
https://playerservices.streamtheworld.com/api/livestream-redirect/HITRADIO_CITY_PRAHA.aac

### 64
https://playerservices.streamtheworld.com/api/livestream-redirect/HITRADIO_CITY_PRAHAAAC.aac

---

## Free Radio
### 128
https://icecast8.play.cz/freeradio128.mp3

### 64
https://icecast6.play.cz/freeradio64.mp3

### 32
https://icecast8.play.cz/freeradio32.mp3

---

## Radio Hey
### 128
https://icecast3.play.cz/hey-radio128.mp3

### 64
https://icecast8.play.cz/hey-radio64.mp3

---

## Rádio Impuls
### 128
https://icecast1.play.cz/impuls128.mp3


### 64
https://icecast5.play.cz/impuls64.mp3

---

## Evropa2
### 128
https://ice.actve.net/fm-evropa2-128