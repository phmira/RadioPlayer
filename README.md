# Radio Player
## Description
Website for listening to Online Radios ~~at my workplace~~. (I just left the company so i will strip this from local sources managed by my previous workplace.)
I wanted single file so i can have it on my desktop without the need of hosting it on webserver. Im czech so i will have mostly czech radios on this "website".

## Disclaimer
This thing is in development. (It may be forever)

## TODO
- Better design
- ~~Dark Theme~~
- ~~Black Theme~~
- ~~Blue Theme~~
- Make browser rember the slected theme and checked autoplay (also bitrate when it will be implemented).
- Make quality selector (changes the bitrate). (Probably will make me rewrite audio source switching)
- Make sources tab (with links to icecast sources and/or official websites).
- Choose a license.
- Maybe show song playing right now (probably hard af for n00b like me).
- Remove few console.log("unnecesary");
- Sort radio staions alphabeticaly.

## Issues
- ~~When switching radio stations reload audio source.~~
- When unpausing radio station reload audio source (It plays cached bit over and over). (For that i would probably need to make my own controls for the player) (or make empty file and name it silence.mp3 and onpause change source to it and on play change it back)
- ~~Add time after link so it seems like new link.~~

## Code Snipppets


### Links (Just for me)
- [language switch](https://www.google.com/search?q=javascript+language+switch&hl=cs&source=hp&ei=K6S9YonFAdT1gQab7KmwAg&iflsig=AJiK0e8AAAAAYr2yO0TX0iXic_dyw-Rj01IMeEqSGGCY&oq=javascrpit+lang&gs_lcp=Cgdnd3Mtd2l6EAEYBTIECAAQDTIECAAQDTIECAAQDTIECAAQDTIECAAQDTIECAAQDTIGCAAQHhAWMgYIABAeEBYyBggAEB4QFjIGCAAQHhAWOgoIABDqAhC0AhBDOhAILhDHARDRAxDqAhC0AhBDOhQIABDqAhC0AhCKAxC3AxDUAxDlAjoFCC4QgAQ6CwgAEIAEELEDEIMBOg4ILhCABBCxAxDHARCjAjoICC4QgAQQsQM6CAgAEIAEELEDOggILhCxAxCDAToLCC4QgAQQsQMQgwE6BAgAEEM6BAguEEM6BwgAELEDEEM6DgguEIAEELEDEIMBENQCOgUIABCABDoECAAQCkoFCDsSATFQqQRYkjVghFBoAHAAeACAAdUCiAGFD5IBCDExLjIuMi4xmAEAoAEBsAEK&sclient=gws-wiz)

## License
I dont have one yet.